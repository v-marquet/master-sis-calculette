public class ObjRPL {
    private int value;

	public ObjRPL(int value) {
        this.value = value;
	}

    public void plus(ObjRPL obj) {
        this.value += obj.getValue();
    }

    public void minus(ObjRPL obj) {
        this.value -= obj.getValue();
    }

    public void multiply(ObjRPL obj) {
        this.value *= obj.getValue();
    }

    public void divide(ObjRPL obj) {
        this.value /= obj.getValue();
    }

    public int getValue() {
        return this.value;
    }

    public String toString() {
        return Integer.toString(this.value);
    }
}

