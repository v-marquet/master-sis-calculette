import java.io.*;

public class PileRPL {
	private final static int NB_OBJ_MAX = 5;
	private ObjRPL pile[];
	private int nbObj = 0;  // top stack pointer (first free cell)


    public PileRPL() {
        this(NB_OBJ_MAX);
    }

	public PileRPL(int nb_obj_max) {
		this.pile = new ObjRPL[nb_obj_max];
	}

	public void push(ObjRPL obj) throws StackOverflowException {
        if (nbObj == NB_OBJ_MAX)
            throw new StackOverflowException();
		this.pile[nbObj] = obj;
        nbObj++;
	}

    public ObjRPL pop() throws StackEmptyException {
        if (nbObj == 0)
            throw new StackEmptyException();
        nbObj--;
        return pile[nbObj];
    }

    public void swap() throws StackEmptyException {
        if (nbObj < 2)
            throw new StackEmptyException();
        ObjRPL obj1 = this.pop();
        ObjRPL obj2 = this.pop();
        try {
            this.push(obj1);
            this.push(obj2);
        } catch (StackOverflowException e) {}
    }

    public void drop() throws StackEmptyException {
        this.pop();
    }

    public void plus() throws StackEmptyException {
        if (nbObj < 2)
            throw new StackEmptyException();
        ObjRPL obj1 = this.pop();
        ObjRPL obj2 = this.pop();
            obj1.plus(obj2);
        try {
            this.push(obj1);
        } catch (StackOverflowException e) {}
    }

    public void minus() throws StackEmptyException {
        if (nbObj < 2)
            throw new StackEmptyException();
        ObjRPL obj1 = this.pop();
        ObjRPL obj2 = this.pop();
        obj1.minus(obj2);
        try {
            this.push(obj1);
        } catch (StackOverflowException e) {}
    }

    public void multiply() throws StackEmptyException {
        if (nbObj < 2)
            throw new StackEmptyException();
        ObjRPL obj1 = this.pop();
        ObjRPL obj2 = this.pop();
        obj1.multiply(obj2);
        try {
            this.push(obj1);
        } catch (StackOverflowException e) {}
    }

    public void divide() throws StackEmptyException {
        if (nbObj < 2)
            throw new StackEmptyException();
        ObjRPL obj1 = this.pop();
        ObjRPL obj2 = this.pop();
        obj1.divide(obj2);
        try {
            this.push(obj1);
        } catch (StackOverflowException e) {}
    }

    public String toString() {
        // first, we compute the length of the largest number in the stack
        // because we need to determine the width of the display of the stack
        int width = 5;  // default minimum width
        for (int i=0; i<nbObj; i++)
            if (this.pile[i].toString().length() >= width)
                width = this.pile[i].toString().length() + 1;

        // we draw the top of the stack
        String pile_s = "";
        pile_s += "    ┌─" + new String(new char[width]).replace("\0", "─") + "─┐\n";
        
        // we draw the cells
        for (int i=0; i<NB_OBJ_MAX; i++) {
            if (NB_OBJ_MAX - 1 - i >= nbObj)
                pile_s += "    │ " + new String(new char[width]).replace("\0", " ") + " │\n";  // empty cell
            else
                pile_s += "    │ " + String.format("% " + width + "d", pile[NB_OBJ_MAX - 1 - i].getValue()) + " │\n";

            if (i == NB_OBJ_MAX-1)
                pile_s += "    └─" + new String(new char[width]).replace("\0", "─") + "─┘\n";  // bottom of stack
            else
                pile_s += "    ├─" + new String(new char[width]).replace("\0", "─") + "─┤\n";  // inter-cell line
        }

        return pile_s;
    }

    public static void main(String[] args) {
        PileRPL pile = new PileRPL();
        ObjRPL obj1 = new ObjRPL(3);
        ObjRPL obj2 = new ObjRPL(5);
        try {
            pile.push(obj1);
            pile.push(obj2);
        } catch (StackOverflowException e) {}
        System.out.println(pile.toString());
    }
}

class StackEmptyException extends Exception {
    public StackEmptyException() {};
}

class StackOverflowException extends Exception {
    public StackOverflowException() {};
}

