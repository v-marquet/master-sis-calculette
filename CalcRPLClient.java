import java.net.*;
import java.io.*;


class CalcRPLClient {

    public static void main( String[] args ) {
        String address = new String("127.0.0.1");
        int port = 12345;

        PrintStream sortie;
        Socket socket;

        try {
            // connect to the servers
            socket = new Socket( address, port );

            // connect input from socket to user console
            BufferedReader b_in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            // set socket output
            sortie = new PrintStream( socket.getOutputStream() );

            // get input from stdin and send to socket
            String line;
            BufferedReader br = new BufferedReader( new InputStreamReader( System.in ));

            boolean quit = false;
            while(!quit) {
                while(true) {
                    try {
                        line = b_in.readLine();
                        if (line.contains("EOT")) {
                            if (line.equals("> EOT")) {
                                System.out.print("> ");
                                break;
                            }
                            else
                                break;
                        }
                        else
                            System.out.println(line);
                    } catch (NullPointerException e) {
                        System.out.println("Server closed connection.");
                        System.exit(0);
                    }
                }

                // we read input from keyboard
                line = br.readLine();
                if (line.equals("exit"))
                    break;
                else
                    sortie.println(line);
            }

            socket.close();
        } catch( UnknownHostException e ) {
            System.out.println( "destinataire inconnu" );
        } catch( IOException e ) {
            System.out.println( "problème d'entrée/sortie" );
            e.printStackTrace();
        }
    }
}
