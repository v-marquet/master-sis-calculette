import java.io.*;
import java.util.Scanner;
import java.net.ServerSocket;
import java.net.Socket;


public class CalcRPLServer {
    private PileRPL pile;
    private static final int port = 12345;

    public CalcRPLServer() {
        this.pile = new PileRPL();
    }

    public static void main(String[] args) {
        CalcRPLServer calc = new CalcRPLServer();
        calc.serve();
    }

    public void serve() {
        try {
            ServerSocket serversocket = new ServerSocket(port);

            boolean quit = false;
            while(!quit) {
                Socket socket = serversocket.accept();
                System.out.println("Le client s'est connecté.");
                
                try {
                    menu(new BufferedReader(new InputStreamReader(socket.getInputStream())), 
                         new PrintStream(new BufferedOutputStream(socket.getOutputStream())));
                } catch (Exception e) {

                }
                
                System.out.println("Le client s'est déconnecté.");
                socket.close();             
            }
                
            serversocket.close();
        } catch (Exception ex) {
            System.out.println("ERREUR: port non disponible / client déconnecté / ...");
        }
    }

    public void menu(BufferedReader in, PrintStream out) throws IOException {
        while(true) {
            out.println("Do you want to get input from keyboard or from a file? (enter number)");
            out.println("[1] keyboard");
            out.println("[2] file");
            out.println("EOT");
            out.flush();
            try {
                int i = Integer.parseInt(in.readLine().trim());
                if (i == 1) {
                    while(true) {
                        out.println("Do you want to record the session to a file? [y|n]: ");
                        out.println("EOT");
                        out.flush();
                        char c = in.readLine().trim().charAt(0);
                        if (c == 'n') {
                            this.interactiveShell(in, out);
                        }
                        else if (c == 'y') {
                            out.println("Enter filename: ");
                            out.println("EOT");
                            out.flush();
                            String filename = in.readLine().trim();
                            File file = new File(filename);
                            try {
                                file.createNewFile();
                            } catch (IOException e) {
                                out.println("error: can't create new file.");
                                out.println("EOT");
                                out.flush();
                            }
                            try {
                                this.interactiveShell(in, out, new FileOutputStream(file));
                            } catch (FileNotFoundException e) {
                                out.println("error: can't save logs to file.");
                                out.println("EOT");
                                out.flush();
                            }
                        }
                        else {
                            out.println("bad option.");
                            out.println("EOT");
                            out.flush();
                        }
                    }
                }
                else if (i == 2) {
                    out.println("Enter filename: ");
                    out.println("EOT");
                    out.flush();
                    String filename = in.readLine().trim();
                    try {
                        this.interactiveShell(new BufferedReader(new FileReader(filename)), out);
                    } catch (FileNotFoundException e) {
                        out.println("File not found.");
                        out.println("EOT");
                        out.flush();
                    }
                }
                else
                    break;

            } catch (NumberFormatException e) {
                out.println("bad option.");
                out.println("EOT");
                out.flush();
            }
        }
    }

    public void interactiveShell(BufferedReader in, PrintStream out) throws IOException {
        this.interactiveShell(in, out, null);
    }

    public void interactiveShell(BufferedReader in, PrintStream out, FileOutputStream logfile) throws IOException {
        out.println("Enter 'h' to display the help. Enter 'q' to quit.");
        out.print("> ");
        out.println("EOT");
        out.flush();

        String cmd;

        PrintWriter pw = null;
        if (logfile != null)
            pw = new PrintWriter(logfile);

        boolean quit = false;
        while(!quit) {
            cmd = in.readLine();
            if (cmd == null)  // when reading from file, if we reach the end
                throw new IOException();
            else
                cmd = cmd.trim();
            switch( cmd.split(" ")[0] ) {
                case "":
                    break;
                case "pop":
                case "drop":
                    try {
                        this.pile.drop();
                    } catch (StackEmptyException e) {
                        out.println("error: not enough elements in stack");
                    }
                    break;
                case "push":
                    if (cmd.split(" ").length != 2) {
                        out.println("error: correct syntax is 'push VALUE'");
                        break;
                    }
                    int i;
                    try {
                        i = Integer.parseInt(cmd.split(" ")[1]);
                        this.pile.push(new ObjRPL(i));
                    } catch (NumberFormatException e) {
                        out.println("error: pushed value must be an int");
                    } catch (StackOverflowException e) {
                        out.println("error: stack is full");
                    }
                    break;
                case "+":
                case "plus":
                    try {
                        this.pile.plus();
                    } catch (StackEmptyException e) {
                        out.println("error: not enough elements in stack");
                    }
                    break;
                case "-":
                case "minus":
                    try {
                        this.pile.minus();
                    } catch (StackEmptyException e) {
                        out.println("error: not enough elements in stack");
                    }
                    break;
                case "*":
                case "x":
                case "multiply":
                    try {
                        this.pile.multiply();
                    } catch (StackEmptyException e) {
                        out.println("error: not enough elements in stack");
                    }
                    break;
                case "/":
                case "divide":
                    try {
                        this.pile.divide();
                    } catch (ArithmeticException ex) {
                        out.println("error: can't divide by 0.");
                    } catch (StackEmptyException e) {
                        out.println("error: not enough elements in stack");
                    }
                    break;
                case "swap":
                    try {
                        this.pile.swap();
                    } catch (StackEmptyException e) {
                        out.println("error: not enough elements in stack");
                    }
                    break;
                case "usage":
                case "aide":
                case "help":
                case "h":
                    out.println("Keywords recognised:");
                    out.println("- Stack manipulation: pop, drop, push VALUE.");
                    out.println("- If you enter only a value, it will be pushed onto the stack.");
                    out.println("- Operations: +, plus, -, minus, *, x, multiply, /, divide.");
                    break;
                case "e":
                case "exit":
                case "q":
                case "quit":
                    out.println("Exiting...");
                    System.exit(0);
                    break;
                default:
                    // by default, we expect a number
                    try {
                        i = Integer.parseInt(cmd);
                        this.pile.push(new ObjRPL(i));
                    } catch (NumberFormatException e) {
                        out.println("Unknown command");
                    } catch (StackOverflowException e) {
                        out.println("error: stack is full");
                    }
                    break;
            }

            out.println(this.pile.toString());
            out.print("> ");
            out.println("EOT");
            out.flush();

            if (logfile != null) {
                pw.println(cmd);
                pw.flush();
            }
        } // end while(true)

        System.exit(0);
    }
}

